
package com.inn.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.inn.system"})
@ServletComponentScan({"com.inn.system"})
@EntityScan({"com.inn.system"})
@EnableAutoConfiguration

public class SystemManageMentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SystemManageMentApplication.class, args);
	}

}