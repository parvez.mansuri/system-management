package com.inn.system.repository.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.inn.system.model.Customer;
import com.inn.system.repository.CustomerRepository;
import com.inn.system.utils.GenericJpaDao;

@Repository("CustomerRepositoryImpl")
public class CustomerRepositoryImpl extends GenericJpaDao<Customer> implements CustomerRepository{

	public CustomerRepositoryImpl() {
		super(Customer.class);
	}

}
