package com.inn.system.repository;

import com.inn.system.model.Customer;
import com.inn.system.utils.IGenericDao;

public interface CustomerRepository extends IGenericDao<Customer>{
	

}
