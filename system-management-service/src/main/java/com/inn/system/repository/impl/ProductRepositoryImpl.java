package com.inn.system.repository.impl;

import java.util.Collections;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.inn.system.model.Product;
import com.inn.system.repository.ProductRepository;
import com.inn.system.utils.GenericJpaDao;

@Repository("ProductRepositoryImpl")
public class ProductRepositoryImpl extends GenericJpaDao<Product> implements ProductRepository{

	public ProductRepositoryImpl() {
		super(Product.class);
	}

	public List<Product> findByName(String name){
		try {
			Query query = getEntityManager().createNamedQuery("findByName").setParameter(1, name);
			return query.getResultList();
		} catch (Exception e) {
		return Collections.emptyList();
		}
	}

	
}
