package com.inn.system.repository;

import java.util.List;

import com.inn.system.model.Product;
import com.inn.system.utils.IGenericDao;


public interface ProductRepository extends IGenericDao<Product>{
	
	public List<Product> findByName(String name);
 
}
