package com.inn.system.controller.impl;

import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.inn.system.controller.IProductController;
import com.inn.system.model.Product;
import com.inn.system.service.IProductService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static net.logstash.logback.argument.StructuredArguments.kv;

@RestController

public class ProductController implements IProductController {
	
	
	private Logger logger = LogManager.getLogger(ProductController.class);
	
	@Autowired
	private IProductService productService;
	
	@Override
    public String helloWorld() {
        return "Hello World RESTful with Spring Boot";
    }
	@Override
	public List<Product> getList(){
		
		return productService.listAll();
		
	}
	public String  save(Product product) {
		logger.info("Inside Save", "validateOnSiteAction", kv("Name", product.getName()));
		productService.save(product);
		return "success";
		
	}
	public Product findById(int id){
		try {
		return productService.get(id);
		}catch (NoSuchElementException e) {
			return new Product();
		}
	}
	
	public List<Product> findByName(String name ){
		String nm = name;
		logger.info("Inside findByName", "validateOnSiteAction", kv("Name", name));
		return productService.findByName(nm) ;
	}
	@Override
	public String update(Product product) {
				productService.update(product);
				return "success";
	}
	@Override
	public String updateProductFromCsv(MultipartFile multipartfile) {
		return "success";
}
}
