package com.inn.system.controller.impl;

import static net.logstash.logback.argument.StructuredArguments.kv;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.inn.system.controller.ICustomerController;
import com.inn.system.model.Customer;
import com.inn.system.service.ICustomerService;


@RestController
public class CustomerController implements ICustomerController{
	
	@Autowired
	ICustomerService customerService;
	
	private Logger logger = LogManager.getLogger(CustomerController.class);
	
		public List<Customer> getAll(){
		logger.info("Inside Method" ,"getAll",kv("getAll", ""));
		return customerService.listAll();
	}
	
}
