package com.inn.system.service;

import java.util.List;

import com.inn.system.model.Customer;

public interface ICustomerService {
	 public List<Customer> listAll();
	 public void save(Customer customer);
	 public Customer get(Integer id);
	 public void delete(Integer id);
	 public List<Customer> getData(String data);
	 public List<Customer> findByName(String name);
}
