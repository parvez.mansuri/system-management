package com.inn.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inn.system.model.Customer;
import com.inn.system.repository.CustomerRepository;
import com.inn.system.service.ICustomerService;


@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	CustomerRepository customerRepo;

	@Override
	public List<Customer> listAll() {
		return customerRepo.findAll();
	}

	@Override
	public void save(Customer customer) {
		customerRepo.create(customer);
	}

	@Override
	public Customer get(Integer id) {
		return customerRepo.findOne(id);
	}

	@Override
	public void delete(Integer id) {
		customerRepo.deleteById(id);

	}

	@Override
	public List<Customer> getData(String data) {
		return customerRepo.findAll();
	}

	@Override
	public List<Customer> findByName(String name) {
		return ((ICustomerService) customerRepo).findByName(name);
	}


}
