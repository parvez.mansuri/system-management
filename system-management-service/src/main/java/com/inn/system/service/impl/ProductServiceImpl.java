package com.inn.system.service.impl;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inn.system.model.Product;
import com.inn.system.repository.ProductRepository;
import com.inn.system.service.IProductService;
 
@Service
@Transactional
public class ProductServiceImpl implements IProductService {
	
    @Autowired
    private ProductRepository repo;
    
     
    public List<Product> listAll() {
        return repo.findAll();
    }
     
    public void save(Product product) {
        repo.create(product);
    }
    public void update(Product product) {
        repo.update(product);
    }
     
    public Product get(Integer id) {
        return repo.findOne(id);
    }
     
    public void delete(Integer id) {
        repo.deleteById(id);
    }

	public List<Product> getData(String data) {
		return repo.findAll();
	}

	@Override
	public List<Product> findByName(String name) {
		
		return repo.findByName(name);
	}

	
	
}
