package com.inn.system.service;

import java.util.List;

import com.inn.system.model.Product;


public interface IProductService{

	 public List<Product> listAll();
	 public void save(Product product);
	 public void update(Product product);
	 public Product get(Integer id);
	 public void delete(Integer id);
	 public List<Product> getData(String data);
	 public List<Product> findByName(String name);
	
}
