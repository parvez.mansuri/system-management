FROM openjdk:8
ADD target/system-management-microservice-1.0.DEV.jar system-management-microservice-1.0.DEV.jar 
EXPOSE 9044
ENTRYPOINT ["java","-jar","system-management-microservice-1.0.DEV.jar"]