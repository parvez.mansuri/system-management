package com.inn.system.model;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringEscapeUtils;

@Data
@Entity
@Table(name = "product")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "customer" })
public class Product implements Serializable  {

	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;
    @Column
    private float price;
    @JoinColumn(name="customer_id",referencedColumnName="id")
    @ManyToOne
    private  Customer customer;
//public static void main(String[] args) {
////	Product pd = new Product();
////	System.out.println(pd instanceof Serializable);
//	String linkname = "JTI_M_00560&#x23;ZTE_1360";
//	linkname = StringEscapeUtils.unescapeHtml(linkname.replaceAll("\\<[^>]*>",""));
//	System.out.println(linkname);
//	
//}
}

