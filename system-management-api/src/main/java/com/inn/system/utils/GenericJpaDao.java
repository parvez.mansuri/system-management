package com.inn.system.utils;

import java.io.Serializable;

public class GenericJpaDao< T extends Serializable >
 extends AbstractJpaDAO< T > implements IGenericDao< T >{

	public GenericJpaDao(Class<T> class1) {
		super(class1);
	}
}
