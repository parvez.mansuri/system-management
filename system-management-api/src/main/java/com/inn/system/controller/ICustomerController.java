package com.inn.system.controller;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.inn.system.model.Customer;

@RequestMapping(path  = "Product")
public interface ICustomerController {
	
	@GetMapping(path = "search")
	public List<Customer> getAll();
}
