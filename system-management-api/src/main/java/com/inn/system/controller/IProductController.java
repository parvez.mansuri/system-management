package com.inn.system.controller;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.inn.system.model.Product;

@RequestMapping(path  = "Product")
public interface IProductController {
	
	@GetMapping(path = "list")
	public List<Product> getList();
	
	@GetMapping(path = "hello")
	public String helloWorld();
		
	@PostMapping(path = "save")
	public String  save(@RequestBody Product product);
	
	@PostMapping(path = "update")
	public String  update(@RequestBody Product product);
	
	@RequestMapping(path = "findById/{id}")
	public Product findById(@PathVariable("id") int id);
	
	@GetMapping(path = "findByName")
	public List<Product> findByName(@RequestParam("name") String name);
	
	@PostMapping(path="updateProductFromCsv")
	public String updateProductFromCsv(@RequestParam("fileData") MultipartFile mupltipartData);
}
